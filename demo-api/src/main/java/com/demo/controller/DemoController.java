package com.demo.controller;

import com.demo.controller.dto.DemoDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/demo")
public interface DemoController {

	@GetMapping("/get")
	DemoDto getDemo();
}
