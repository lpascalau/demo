package com.demo.dao.impl;

import com.demo.controller.dto.DemoDto;
import com.demo.dao.api.DemoService;
import org.springframework.stereotype.Service;

@Service
public class DemoServiceImpl implements DemoService {
	@Override
	public DemoDto getDemo() {
		DemoDto demoDto = new DemoDto();
		demoDto.setExternalId("demoExternalId");
		return demoDto;
	}
}
