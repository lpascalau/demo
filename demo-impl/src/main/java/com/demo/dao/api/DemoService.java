package com.demo.dao.api;

import com.demo.controller.dto.DemoDto;

public interface DemoService {
	DemoDto getDemo();
}
