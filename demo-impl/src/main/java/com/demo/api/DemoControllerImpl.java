package com.demo.api;

import com.demo.controller.DemoController;
import com.demo.controller.dto.DemoDto;
import com.demo.dao.api.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoControllerImpl implements DemoController {
	@Autowired
	private DemoService demoService;

	@Override
	public DemoDto getDemo() {
		return demoService.getDemo();
	}
}
